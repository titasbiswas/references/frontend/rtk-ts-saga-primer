import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { decrement, increment, incrementByAmount } from "./counterSlice";
import styles from "./Counter.module.css";
import { RootState } from "../../app/rootReducer";
import {
  INCREMENT_BY_AMOUNT_ASYNC_REQ,
  INCREMENT_BY_AMOUNT_REQ,
} from "../counter/actions";
import { PayloadAction } from "@reduxjs/toolkit";

export function Counter() {
  const count = useSelector((state: RootState) => state.counter.value);
  const dispatch = useDispatch();
  const [incrementAmount, setIncrementAmount] = useState("2");

  return (
    <div>
      <div className={styles.row}>
        <button
          className={styles.button}
          aria-label='Increment value'
          onClick={() => dispatch(increment())}
        >
          +
        </button>
        <span className={styles.value}>{count}</span>
        <button
          className={styles.button}
          aria-label='Decrement value'
          onClick={() => dispatch(decrement())}
        >
          -
        </button>
      </div>
      <div className={styles.row}>
        <input
          className={styles.textbox}
          aria-label='Set increment amount'
          value={incrementAmount}
          onChange={(e) => setIncrementAmount(e.target.value)}
        />
        <button
          className={styles.button}
          onClick={() => {
            const action: PayloadAction<number> = {
              type: INCREMENT_BY_AMOUNT_REQ,
              payload: Number(incrementAmount) || 0,
            };
            dispatch(action);
          }}
        >
          Add Amount
        </button>
        <button
          className={styles.asyncButton}
          onClick={() => {
            const action: PayloadAction<number> = {
              type: INCREMENT_BY_AMOUNT_ASYNC_REQ,
              payload: Number(incrementAmount) || 0,
            };
            dispatch(action);
          }}
        >
          Add Async
        </button>
      </div>
    </div>
  );
}
