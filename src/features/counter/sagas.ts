import { PayloadAction } from '@reduxjs/toolkit'
import {put, take, takeEvery, call, delay} from 'redux-saga/effects'
import { INCREMENT_BY_AMOUNT_ASYNC_REQ, INCREMENT_BY_AMOUNT_REQ } from './actions'
import { increment, decrement, incrementByAmount } from './counterSlice'

function* watchAllCounterActions(){
    yield takeEvery(INCREMENT_BY_AMOUNT_REQ, incrementByAmountWorker)
    yield takeEvery(INCREMENT_BY_AMOUNT_ASYNC_REQ, incrementByAmountAsyncWorker)

}

function* incrementByAmountWorker(action:PayloadAction<number>) {
    yield put(incrementByAmount(action.payload))
}

function* incrementByAmountAsyncWorker(action:PayloadAction<number>) {
    yield delay(5000)
    yield put(incrementByAmount(action.payload))
}

export default watchAllCounterActions
